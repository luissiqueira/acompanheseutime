#coding=utf-8
'''
Created on 30/04/2013

@author: luissiqueira
'''
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from futsilex.core.models import Clube, Competicao, ClubeEmFase, Partida,\
    FaseCampeonato
import datetime
import urllib2, json

def cadastrar_obter_competicao(j):
    dict = {
            'nome' : j['nome'],
            'slug' : j['slug'],
        }
    if Competicao.objects.filter(**dict).exists():
        competicao = Competicao.objects.get(**dict)
    else :
        dict['url_noticias'] = 'http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/'+ str(j['slug']) +'/feed.rss'
        dict['ativo'] = True
        dict['slug_editorial'] = j['slug_editorial']
        dict['regulamento'] = j['regulamento']
        dict['id_ge'] = j['edicaocampeonato_id']
        dict['data_inicial'] = j['data_inicio']
        dict['data_inicial'] = datetime.date(int(dict['data_inicial'].split('-')[0]),int(dict['data_inicial'].split('-')[1]),int(dict['data_inicial'].split('-')[2]))
        dict['data_final'] = j['data_fim']
        dict['data_final'] = datetime.date(int(dict['data_final'].split('-')[0]),int(dict['data_final'].split('-')[1]),int(dict['data_final'].split('-')[2]))
        
        competicao = Competicao(**dict)
        competicao.save()

    return competicao

def cadastrar_clubes(i):
    for k,v in i.items():
        info = {'nome' : v['nome'], 
                'slug' : v['slug'],  }
        if not Clube.objects.filter(**info).exists():
            info['escudo'] = v['escudo']
            info['sigla'] = v['sigla'] 
            info['apelido'] = v['apelido']
            info['escudo_pequeno'] = v['escudo_pequeno']
            info['escudo_medio'] = v['escudo_medio']
            info['escudo_grande'] = v['escudo_grande']
            info['url_noticias'] = 'http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/'+str(v['slug'])+'/feed.rss'
            info['id_ge'] = k
            c = Clube(**info)
            c.save()
            
def cadastrar_fases(i,competicao):
    for v in i['fases']:
        if v['nome'] == 'mata-mata' and v.get('subtipo','') == 'grupo':
            tipo = "mata-mata-agrupado"
        #elif v.get('subtipo','') == 'simples':
        else:
            tipo = v['tipo_classificacao']
        if tipo != "mata-mata-agrupado":
            if not FaseCampeonato.objects.filter(id_ge=int(v['fase_id'])).exists():
                dict = {
                    'campeonato' : competicao,
                    'atual' : v['atual'],
                    'id_ge' : v['fase_id'],
                    'nome' : v['nome'],
                    'slug' : v['slug'],
                    'tipo_classificacao' : v['tipo_classificacao']
                }
                f = FaseCampeonato(**dict)
                f.save()
        else:
            for k in v[tipo]:
                if not FaseCampeonato.objects.filter(id_ge=int(k['fase_id'])).exists():
                    dict = {
                        'campeonato' : competicao,
                        'atual' : k['atual'],
                        'id_ge' : k['fase_id'],
                        'nome' : k['nome'],
                        'slug' : k['slug'],
                        'tipo_classificacao' : k['tipo_classificacao']
                    }
                    f = FaseCampeonato(**dict)
                    f.save()

def cadastrar_clubes_em_fases():
    pass

def consultar_ge(request):
    # url = 'http://globoesporte.globo.com/dynamo/futebol/campeonato/campeonato-brasileiro-b/brasileiro-serieb-2013/classificacao.json'
    # url = 'http://globoesporte.globo.com/dynamo/futebol/campeonato/campeonato-brasileiro/brasileirao2013/classificacao.json'
    
    urls = ['http://globoesporte.globo.com/dynamo/futebol/campeonato/campeonato-brasileiro/brasileirao2013/classificacao.json', \
            'http://globoesporte.globo.com/dynamo/futebol/campeonato/campeonato-brasileiro-b/brasileiro-serieb-2013/classificacao.json', \
            'http://globoesporte.globo.com/dynamo/futebol/campeonato/copa-do-brasil/copa-do-brasil-2013/classificacao.json']
    
    for url in urls:
        data = urllib2.urlopen(url)
        j = json.load(data)
        
        competicao = cadastrar_obter_competicao(j['edicao_campeonato'])
        #cadastrar_clubes(j['lista_de_jogos']['campeonato']['edicao_campeonato']['equipes'])
        cadastrar_fases(j['edicao_campeonato'],competicao)
        cadastrar_clubes_em_fases()
        
        for fase in j['edicao_campeonato']['fases']:
            if fase['nome'] == 'mata-mata' and fase.get('subtipo','') == 'grupo':
                tipo = "mata-mata-agrupado"
            #elif fase.get('subtipo') == 'simples':
            else:
                tipo = fase['tipo_classificacao']
            if not "mata" in tipo:
                f = FaseCampeonato.objects.get(id_ge=fase['fase_id']) if FaseCampeonato.objects.filter(id_ge=fase['fase_id']).exists() else False
                if f:
                    i = fase[tipo][0]['classificacoes'][0]['classificacao']
                    pos = 1
                    for v in i:
                        equipe_id = v['equipe_id']
                        equipe = Clube.objects.get(id_ge=equipe_id)
                        if not ClubeEmFase.objects.filter(clube=equipe,fase=f).exists():
                            dict = {
                                'clube' : equipe,
                                'fase' : f,
                                'pontos': v['pontos'],
                                'jogos': v['jogos'],
                                'vitorias': v['vitorias'],
                                'derrotas': v['derrotas'],
                                'empates': v['empates'],
                                'gols_contra': v['gols_contra'],
                                'gols_pro': v['gols_pro'],
                                'saldo_gols': v['saldo_gols'],
                                'posicao': pos,
                                'aproveitamento': v['aproveitamento'],
                            }
                            c = ClubeEmFase(**dict)
                            c.save()
                            pos += 1
            
            
        try:
            i = j['lista_de_jogos']['campeonato']['edicao_campeonato']['fases'][0]['jogos']
            for v in i:
                d,m,y = [int(x) for x in v['data_original'].split('-')]
                if v['hora']:
                    h,M = [int(x) for x in v['hora'].split('h')]
                    data = datetime(y,m,d,h,M)
                else:
                    data = datetime(y,m,d)
                equipe_mandante = Clube.objects.get(id_ge=v['equipe_mandante'])
                equipe_visitante = Clube.objects.get(id_ge=v['equipe_visitante'])
                dict = {
                    'data' : data,
                    'clube_casa' : equipe_mandante,
                    'clube_visitante' : equipe_visitante,
                    'competicao' : competicao,
                    'rodada' : v['rodada'],
                    'local' : v['local_jogo'],
                }
                if not Partida.objects.filter(**dict).exists():
                    partida = Partida(**dict)
                    partida.save()
        except:
            pass
        
    j = i = []
    return render_to_response('index.html',
                        context_instance=RequestContext(request),
                        mimetype="text/html; charset=utf-8;")