#coding=utf-8
'''
Created on 29/04/2013

@author: luissiqueira
'''
from django.db import models

class Competicao(models.Model):
    nome = models.CharField(max_length=50, verbose_name="Nome")
    slug = models.CharField(max_length=50, verbose_name="Slug", null=True, blank=True)
    ordem = models.IntegerField(verbose_name="Ordem de exibição", default=0)
    url_padrao = models.CharField(max_length=150, verbose_name="URL Padrão", null=True, blank=True)
    url_noticias = models.CharField(max_length=150, verbose_name="URL Notícias", null=True, blank=True)
    url_jogos = models.CharField(max_length=150, verbose_name="URL Jogos", null=True, blank=True)
    url_tempo_real = models.CharField(max_length=150, verbose_name="URL Tempo Real", null=True, blank=True)
    url_mensagens = models.CharField(max_length=150, verbose_name="URL Mensagens", null=True, blank=True)
    ativo = models.BooleanField(verbose_name="Campeonato Ativo?")
    data_inicial = models.DateField(verbose_name="Início do Campeonato", null=True, blank=True)
    data_final = models.DateField(verbose_name="Fim do Campeonato", null=True, blank=True)
    id_ge = models.CharField(max_length=10, verbose_name="Identificador GE", null=True, blank=True)
    regulamento = models.TextField(verbose_name="Regulamento",null=True,blank=True)
    rodada_atual = models.CharField(max_length=3, verbose_name="Rodada Atual",default="0")  
    slug_editorial = models.CharField(max_length=50, verbose_name="Slug Editorial", null=True, blank=True)
    
    def __unicode__(self):
        return self.nome
    
    class Meta:
        verbose_name = "Competição"
        verbose_name_plural = "Competições"
        ordering = ['ordem','nome']

class FaseCampeonato(models.Model):
    campeonato = models.ForeignKey(Competicao, verbose_name="Edição", related_name="edicao")
    atual = models.BooleanField(verbose_name="Fase Atual?")
    nome = models.CharField(max_length=50, verbose_name="Nome")
    slug = models.CharField(max_length=50, verbose_name="Slug", null=True, blank=True)
    tipo_classificacao = models.CharField(max_length=50, verbose_name="Tipo Classificação", null=True, blank=True)
    id_ge = models.CharField(max_length=10, verbose_name="Identificador GE", null=True, blank=True)
    
    def __unicode__(self):
        return str(self.campeonato) + ' -> ' + str(self.nome)
    
class Clube(models.Model):
    nome = models.CharField(max_length=50, verbose_name="Nome")
    apelido = models.CharField(max_length=50, verbose_name="Apelido", null=True, blank=True)
    sigla = models.CharField(max_length=50, verbose_name="Sigla", null=True, blank=True)
    slug = models.CharField(max_length=50, verbose_name="Slug", null=True, blank=True)
    ordem = models.IntegerField(verbose_name="Ordem de exibição", default=0)
    escudo = models.CharField(max_length=100, verbose_name="Escudo", null=True, blank=True)
    escudo_grande = models.CharField(max_length=100, verbose_name="Escudo Grande", null=True, blank=True)
    escudo_medio = models.CharField(max_length=100, verbose_name="Escudo Médio", null=True, blank=True)
    escudo_pequeno = models.CharField(max_length=100, verbose_name="Escudo Pequeno", null=True, blank=True)
    url_noticias = models.CharField(max_length=150, verbose_name="URL Notícias", null=True, blank=True)
    # http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/{{slug}}/feed.rss
    ativo = models.BooleanField(verbose_name="Clube Ativo?",default=True)
    id_ge = models.CharField(max_length=10, verbose_name="Identificador GE", null=True, blank=True)
    
    def __unicode__(self):
        return self.nome
    
    class Meta:
        verbose_name = "Clube"
        verbose_name_plural = "Clubes"
        ordering = ['ordem','nome']

class ClubeEmFase(models.Model):
    clube = models.ForeignKey(Clube, verbose_name="Clube", related_name="clube")
    fase = models.ForeignKey(FaseCampeonato, verbose_name="Fase Campeonato", related_name="competicao")
    ano = models.IntegerField(verbose_name="Ano da Competição",default=2013)
    ativo = models.BooleanField(verbose_name="Participação Ativa?",default=True)
    posicao = models.IntegerField(verbose_name="Classificação", default = 0)
    pontos = models.CharField(max_length=2, verbose_name="Pontos")
    jogos = models.CharField(max_length=2, verbose_name="Jogos")
    vitorias = models.CharField(max_length=2, verbose_name="Vitórias")
    empates = models.CharField(max_length=2, verbose_name="Empates")
    derrotas = models.CharField(max_length=2, verbose_name="Derrotas")
    gols_pro = models.CharField(max_length=2, verbose_name="Gols Pro")
    gols_contra = models.CharField(max_length=2, verbose_name="Gols Contra")
    saldo_gols = models.CharField(max_length=3, verbose_name="Saldo de Gols")
    aproveitamento = models.CharField(max_length=5, verbose_name="Aproveitamento")
    
    def __unicode__(self):
        return str(self.clube) + " - " + str(self.fase)
    
    class Meta:
        verbose_name = "Clube em Fase"
        verbose_name_plural = "Clubes em Fases"
        ordering = ['fase','posicao']

class Noticia(models.Model):
    clube = models.ForeignKey(Clube, verbose_name="Clube", related_name="Clube", null=True, blank=True)
    competicao = models.ForeignKey(Competicao, verbose_name="Competição", related_name="Competição", null=True, blank=True)
    titulo = models.CharField(max_length=50, verbose_name="Titulo da Notícia")
    data = models.DateField(verbose_name="Data da Notícia", null=True, blank=True)
    url = models.CharField(max_length=150,verbose_name="Url da Notícia", null=True, blank=True)
    conteuto = models.TextField(verbose_name="Notícia", null=True, blank=True)
    externo = models.BooleanField(verbose_name="Conteúdo externo?")
    
    def __unicode__(self):
        return self.titulo
    
    class Meta:
        verbose_name = "Notícia"
        verbose_name_plural = "Notícias"
        ordering = ['-data']

class Partida(models.Model):
    fase = models.ForeignKey(FaseCampeonato, verbose_name="Fase Competição",null=True,blank=True)
    data = models.DateTimeField(verbose_name="Data da Partida")

    clube_casa = models.ForeignKey(Clube, verbose_name="Clube da Casa", related_name="Clube da Casa")
    clube_visitante = models.ForeignKey(Clube, verbose_name="Clube Visitante", related_name="Clube Visitante")

    placar_casa = models.CharField(max_length=2, verbose_name="Placar Casa", null=True, blank=True, default="0")
    placar_visitante = models.CharField(max_length=2, verbose_name="Placar Visitante", null=True, blank=True, default="0")
    placar_prorrogacao_casa = models.CharField(max_length=2, verbose_name="Placar Casa (prorrogação)", null=True, blank=True)
    placar_prorrogacao_visitante = models.CharField(max_length=2, verbose_name="Placar Visitante (prorrogação)", null=True, blank=True)
    placar_penalti_casa = models.CharField(max_length=2, verbose_name="Placar Casa (pênalti)", null=True, blank=True)
    placar_penalti_casa = models.CharField(max_length=2, verbose_name="Placar Visitante (pênalti)", null=True, blank=True)
    
    rodada = models.IntegerField(verbose_name="Rodada",null=True,blank=True)
    local = models.CharField(max_length=150,verbose_name="Local da Partida",null=True,blank=True)
    tempo_real = models.BooleanField(verbose_name="Tempo real disponível?")
    url_tempo_real = models.CharField(max_length=150, verbose_name="URL Tempo Real", null=True, blank=True)
    url_mensagens = models.CharField(max_length=150, verbose_name="URL Mensagens", null=True, blank=True)
    total_lances = models.IntegerField(verbose_name="Total de Lances", default=0)
    id_ge = models.CharField(max_length=10, verbose_name="Identificador GE", null=True, blank=True)
    
    def __unicode__(self):
        return str(self.clube_casa) + " x " + str(self.clube_visitante) + " (" + str(self.competicao) + " - " + str(self.data.date().strftime('%d/%m/%Y')) + ")"
    
    class Meta:
        verbose_name = "Partida"
        verbose_name_plural = "Partidas"
        ordering = ['-data']

class Lance(models.Model):
    partida = models.ForeignKey(Partida, verbose_name="Partida")
    periodo = models.CharField(max_length=15, verbose_name="Período", null=True, blank=True, default="Pré-Jogo")
    momento = models.CharField(max_length=5, verbose_name="Tempo de Jogo", null=True, blank=True)
    texto = models.CharField(max_length=255, verbose_name="Lance")
    lance_id = models.CharField(max_length=30, verbose_name="Identificador do Lance")
    operacao = models.CharField(max_length=30, verbose_name="Operação")
    
    def __unicode__(self):
        return str(self.momento) + ' - ' + str(self.periodo) + ' : ' + str(self.texto)[:20]
    
    class Meta:
        verbose_name = "Lance"
        verbose_name_plural = "Lances"
        ordering = ['lance_id','id']