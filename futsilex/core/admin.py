'''
Created on 29/04/2013

@author: luissiqueira
'''
from futsilex.core.models import Clube, ClubeEmFase, Competicao, Partida, FaseCampeonato, Lance, Noticia
from django.contrib import admin
class ClubeAdmin(admin.ModelAdmin):
    list_display = ['nome','apelido','sigla','slug','id_ge','ativo']
    list_filter = ['ativo']
    search_fields = ['nome','sigla','slug','url_noticias','id_ge']

class CompeticaoAdmin(admin.ModelAdmin):
    list_display = ['nome','slug','ativo']
    list_filter = ['ativo']
    search_fields = ['nome','slug','url_noticias','url_jogos']

class ClubeCompeticaoAdmin(admin.ModelAdmin):
    list_display = ['clube','competicao','posicao','ano', 'ativo']
    list_filter = ['ativo','competicao']
    search_fields = ['clube','competicao','ano']

class PartidaAdmin(admin.ModelAdmin):
    list_display = ['data','clube_casa','placar','clube_visitante','competicao','rodada','local']
    list_filter = ['competicao','rodada']
    search_fields = ['clube_casa__nome','clube_visitante__nome','competicao__nome','rodada','local']
    ordering = ['data']
    
admin.site.register(Clube, ClubeAdmin)
admin.site.register(Competicao)
admin.site.register(ClubeEmFase)
admin.site.register(Partida)
admin.site.register(FaseCampeonato)
admin.site.register(Lance)
admin.site.register(Noticia)