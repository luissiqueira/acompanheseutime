from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'futsilex.views.home', name='home'),
    # url(r'^futsilex/', include('futsilex.foo.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^competicoes/', include('futsilex.competicoes.urls')),
    url(r'^clubes/', include('futsilex.clubes.urls')),
)
