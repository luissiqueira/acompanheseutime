#coding=utf-8
'''
Created on 01/05/2013

@author: luissiqueira
'''
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.core import serializers
from futsilex.core.models import Clube

def creat_initial(request):
    data = serializers.serialize("json", Clube.objects.all())
    print data
    return render_to_response('initial_data.html',locals(),
                        context_instance=RequestContext(request),
                        mimetype="text/html; charset=utf-8;")